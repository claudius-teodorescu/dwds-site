const aggregated_indexes_metadata =
    `
    prefix i: <https://kuberam.ro/ontologies/text-index#>

    select ?default_index_metadata_url ?index_description
    where {
        ?default_index_metadata_url i:isDefaultIndex "true" .
        ?index_metadata_url i:metadataFor ?index_url .
        ?index_metadata_url i:title ?index_title .
        bind(concat(str(?index_url), '=', ?index_title, '=', str(?index_metadata_url)) as ?index_description)
    }
`;

const character_normalization_mappings = (index_metadata_url) =>
    `
    prefix i: <https://kuberam.ro/ontologies/text-index#>

    select distinct ?character_normalisation_mapping
    where {
        <${index_metadata_url}> i:characterNormalisationMapping ?character_normalisation_mapping
    }
`;

export {
    aggregated_indexes_metadata,
    character_normalization_mappings,
};
