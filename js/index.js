import StaticSearchEngine from "https://claudius-teodorescu.gitlab.io/index-search-engine/index.js";

const static_search_engine = new StaticSearchEngine();
static_search_engine.addEventListener("static-search-engine:loading-time", (event) => {
    document.querySelector("output#engine-loading-time").value = event.detail;
});
static_search_engine.addEventListener("static-search-engine:index-size", (event) => {
    document.querySelector("output#index-size").value = event.detail;
});

await static_search_engine.init("https://claudius-teodorescu.gitlab.io/dwds-data/peritext/indexes/_aggregated/index.tar.gz");
//await static_search_engine.init("https://localhost:8080/index.tar.gz");

let index_searcher = static_search_engine.searcher;

// use the metadata for the user interface
let options = [`<option value="">--Select an index--</option>`];
static_search_engine.index_metadata.forEach((index_metadatum, i) => {
    let selected = "";

    if (i === 0) {
        selected = `selected="true"`;
    }

    options.push(`<option value="${i}" ${selected}>${index_metadatum.index_title}</option>`);
});
document.querySelector("select#index-select").innerHTML = options.join("");

document.addEventListener("input", event => {
    let target = event.target;

    if (target.matches("input#search-string")) {
        document.querySelector("div#simple-search-result").innerHTML = "";
        let search_string = target.value;
        let index_position = document.querySelector("select#index-select").value;

        let heading_search_start = performance.now();
        let processed_search_string = static_search_engine.process_search_string(index_position, search_string);
        let headings = index_searcher.headings_by_heading_regex(index_position, `.*${processed_search_string}.*`);
        let heading_search_stop = performance.now();

        let headings_number = headings.size;
        let search_result_metadata = `Found ${new Intl.NumberFormat('en-US').format(headings_number)} items in ${heading_search_stop - heading_search_start} miliseconds.`;
        document.querySelector("output#simple-search-result-metadata").value = search_result_metadata;
        if (headings_number < 10000) {
            let headings_dom_string = [];
            headings.forEach((id, heading) => headings_dom_string.push(`<div data-heading-id="${id}" data-heading="${heading}">${heading}</div>`));

            document.querySelector("div#simple-search-result").innerHTML = headings_dom_string.join("");
        }
    }

    if (target.matches("select#index-select")) {
        document.querySelector("input#search-string").value = "";
        document.querySelector("div#simple-search-result").innerHTML = "";
    }
});

document.addEventListener("click", (event) => {
    let target = event.target;

    if (target.matches("div#simple-search-result > div")) {
        let heading_id = target.dataset.headingId;
        let index_position = document.querySelector("#index-select").value;

        let locator = Array.from(index_searcher.locators_by_heading_id(index_position, heading_id).keys())[0];
        console.log("locator = " + locator);

        window.open(locator, '_blank').focus();
    }
});
